from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.enums import TA_CENTER

from reportlab.lib import colors

author_style = ParagraphStyle('author_style')
author_style.textColor = 'white'
author_style.fontSize = 40
author_style.fontName = 'Helvetica'

cover_title = ParagraphStyle('cover_title')
cover_title.textColor = 'white'
cover_title.fontSize = 70
cover_title.fontName = 'Helvetica'

cover_subtitle = ParagraphStyle('cover_subtitle')
cover_subtitle.textColor = 'white'
cover_subtitle.fontSize = 50
cover_subtitle.fontName = 'Helvetica'

content_title =  ParagraphStyle('content_title')
content_title.fontSize = 60
content_title.fontName = 'Helvetica-Bold'
content_title.textColor = colors.HexColor('#cc0066')

content_footer =  ParagraphStyle('content_footer')
content_footer.fontSize = 40
content_footer.leading = 40
content_footer.alignment = TA_CENTER
content_footer.textColor = colors.HexColor('#000000')

general_text =  ParagraphStyle('content_footer')
general_text.fontSize = 40
general_text.leading = 60
general_text.alignment = TA_CENTER
general_text.textColor = colors.HexColor('#000000')

label = ParagraphStyle('label')
label.textColor = 'black'
label.fontSize = 20
label.alignment = TA_CENTER
label.fontName = 'Times-Bold'

##- Banner for credits at back cover
back_cover_banner = ParagraphStyle('back_cover_banner')
back_cover_banner.textColor = 'white'
back_cover_banner.fontSize = 25
back_cover_banner.fontName = 'Helvetica'