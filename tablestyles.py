from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.enums import TA_CENTER
from reportlab.platypus import TableStyle
from reportlab.lib import colors




def table_style(total_rows = None):
	tablestyle =  [('BACKGROUND',(0,0),(2,0),colors.HexColor('#99004d')),
                                    ('TEXTCOLOR',(0,0),(2,0),colors.HexColor('#ffffff')),
                                    ('FONTSIZE',(0,0),(2,0),22),
                                    ('VALIGN',(0,0),(2,0),'TOP'),
                                    ('ALIGN',(0,0),(2,0),'CENTER'),
                                    ### Items
                                    ('BACKGROUND',(0,1),(2,total_rows),colors.HexColor('#ffccdd')),
                                    ('TEXTCOLOR',(0,1),(2,total_rows),colors.HexColor('#000000')),
                                    ('FONTSIZE',(0,1),(2,total_rows),18),
                                    ('VALIGN',(0,1),(2,total_rows),'MIDDLE'),
                                    ('ALIGN',(0,1),(0,total_rows),'CENTER'),
                                    ('ALIGN',(2,1),(2,total_rows),'CENTER'),
                                    ]
	return tablestyle

def table_style_reputation(total_rows = None):
	tablestyle =  [('BACKGROUND',(0,0),(4,0),colors.HexColor('#99004d')),
                                    ('TEXTCOLOR',(0,0),(4,0),colors.HexColor('#ffffff')),
                                    ('FONTSIZE',(0,0),(4,0),22),
                                    ('VALIGN',(0,0),(4,0),'TOP'),
                                    ('ALIGN',(0,0),(4,0),'CENTER'),
                                    ### Items
                                    ('BACKGROUND',(0,1),(4,total_rows),colors.HexColor('#ffccdd')),
                                    ('TEXTCOLOR',(0,1),(4,total_rows),colors.HexColor('#000000')),
                                    ('FONTSIZE',(0,1),(4,total_rows),18),
                                    ('VALIGN',(0,1),(4,total_rows),'MIDDLE'),
                                    ('ALIGN',(0,1),(0,total_rows),'CENTER'),
                                    ('ALIGN',(2,1),(4,total_rows),'CENTER'),
                                    ]
	return tablestyle
