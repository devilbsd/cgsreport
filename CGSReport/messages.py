##- For decoupling messages text from the engine, this is to make easier the translation of the text
import json
from collections import namedtuple

class Messages:
	'''each fucntion represents a template and will return a named tubled with all the tags'''
	
	def __init__(self, config = None ):
		self.lang = config['language']
		##- Load system message output
		with open("CGSReport/messages/system.json") as system_json:
			self.system_msgs = json.load(system_json)
			
		with open("CGSReport/messages/phishing.json") as phishing_json:
			self.phishing_msgs = json.load(phishing_json)
			
		with open("CGSReport/messages/covers.json") as covers_json:
			self.covers_msgs = json.load(covers_json)
		
	def system(self):
		##- Output messages to the console
		
		starting = self.system_msgs['starting'][self.lang]
		goodbye = self.system_msgs['goodbye'][self.lang]
		
		connection_failed = self.system_msgs['connection_failed'][self.lang]
		
		
		Msgs = namedtuple('Messages',['starting',
									  'goodbye',
									  'connection_failed'])
		
		msg = Msgs(starting,
				   goodbye,
				   connection_failed)
		
		return msg


	def phishing(self, data = None):
		##- Output messages to the console
		
		total = str(data.total_of_phishing)
		benigns = data.total_missed_eop
		percent = data.percent_missed_eop
		
		o365_scl_analysis = self.phishing_msgs['o365_scl_analysis'][self.lang] %(total,benigns,percent)
		emails_byscl_barchar_title = self.phishing_msgs['emails_byscl_barchar_title'][self.lang]
		##- Returning Named Tuple
		Msgs = namedtuple('Messages',['o365_scl_analysis', 'emails_byscl_barchar_title'])
		
		msg = Msgs(o365_scl_analysis, emails_byscl_barchar_title)
		
		return msg
	
	def covers(self):
		
		##- Returning Named Tuple
		Msgs = namedtuple('Messages',['cover_subtitle'])
		
		msg = Msgs(self.covers_msgs['cover_subtitle'][self.lang])
		
		return msg