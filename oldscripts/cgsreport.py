## - cgsreport.py takes exported data from custom queries to create a PDF Report
## - Javier Padilla jpadilla@checkpoint.com
import pandas
import plotly.graph_objects as go

##- General Settings

reportid = '3654a529-d1b8-4f9f-970b-b35cea45b5ce'

##- 1st PART - Data Collection
##- Just define the data collection

df = pandas.read_csv('csvdata/login_exitoso.csv')

## - This represents the collumn - that is the topic - we are interested in
mimedata = df['User Email']



##- 2nd Part - Data Processing
##- Creates Maps and delivers a Named Tuple with  to the 3rd Part

##- Create a Named Tuple

allmime = []
types = []
count = []
#colors = ["#204912","#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#8c564b", "#2fbcb2", "#d1c147", "#d14747"]



for mimetype in mimedata:
    allmime.append(mimetype)
    
setmime = set(allmime)


for x in setmime:
    if x in ['Colombia']:
        pass
    else:
        types.append(x)
        count.append(allmime.count(x))
    
print types, count

#fig = go.Figure(data=[go.Pie(labels=types, values=count)])
fig = go.Figure(data=[go.Bar(
            x=types, y=count,
            text=count,
            #color='lifeExp',
            textposition='auto',
        )])
fig.write_image("attachment_in_phishing_by_mimetype.jpg")
#fig.show()



##- #