import json
import time
with open('config.json') as config_file:
    config = json.load(config_file)
from reportdata import Report
report  = Report(config)

##- o365 object is a namedtuble
o365 = report.phishing_analysis_o365()
flogin = report.login_analysis_o365()
malware365 = report.malware_analysis_o365()

cover_subtitle = {}
cover_subtitle['es'] = "Reporte de amenazas en %s para SaaS" % config['customer']
cover_subtitle['en'] = "Threats report at %s's for SaaS" % config['customer']
cover_subtitle['de'] = ""



##- Phishing & Extorsion Section

scl_title = {}
scl_title['es'] = 'Como complementa Check Point a O365 EOP/ATP'
scl_title['en'] = 'How does Check Point helps O365 EOP/ATP'
scl_title['de'] = 'Deutsch'

scl_label = {}
scl_label['es'] = '# de mensajes por "Spam Confidence Level".'
scl_label['en'] = '# of emails by Spam Confidence Level.'
scl_label['de'] = ' Deutsch'


mostphished_title = {}
mostphished_title['es'] = "Top 20 Usuarios que mas reciben Phishing"
mostphished_title['en'] = "Top 20 most Phished users"
mostphished_title['de'] = "Auf Deutsh"

mostphished_label = {}
mostphished_label['es'] = "# de Phishing por Usuario"
mostphished_label['en'] = "# of Phishign emails per user"
mostphished_label['de'] = "Auf Deutsh"

phishers_title = {}
phishers_title['es'] = "Top 20 emails que mas envian Phishing"
phishers_title['en'] = "Top 20 emails sources of Phishing"
phishers_title['de'] = "Auf Deutsh"

phishing_subject_title = {}
phishing_subject_title['es'] = "Top 20 Subject utilizados en Phishing"
phishing_subject_title['en'] = "Top 20 Subjects used on email Phishing"
phishing_subject_title['de'] = "Auf Deutsh"





d1b8={}
d1b8['es'] = 'Malware descubierto por SandBoxing'
d1b8['en'] = 'Malware discovered by SandBox'
d1b8['de'] = 'Von Sandbox entdeckte Malware'

ff9f = {}
ff9f['es'] = 'Tipos de archivos'
ff9f['en'] = 'Filetypes'
ff9f['de'] = 'Tipos de archivos'

d022 = {}
d022['es'] = 'Contenido'
d022['en'] = 'Content'
d022['de'] = 'Inhalt'

phishing_transition = {}
phishing_transition['es'] = 'Phishing & Extorsion'
phishing_transition['en'] = 'Phishing & Extorsion'
phishing_transition['de'] = 'Phishing & Erpressung'



##- Malware section

##- Transition Slide

malware_transition = {}
malware_transition['es'] = 'Malware de dia-cero'
malware_transition['en'] = 'Zero-day Malware'
malware_transition['de'] = 'Zero-day Malware'

##- Malware About

malware_about_title = {}
malware_about_title['es'] = 'Malware en aplicaciones SaaS'
malware_about_title['en'] = 'Malware in SaaS applications'
malware_about_title['de'] = 'Malware'

malware_about_text = {}
malware_about_text['es'] = "Entre %s y %s se detectaron %s archivos adjuntos maliciosos en el correo electr&oacute;nico. Esto es una combinacion de <i>malware</i> y <i>phishing</i> en documentos " %(o365.poc_date_begin, o365.poc_date_ended, malware365.total_of_malware_in_email)
malware_about_text['en'] = "Between %s and %s, %s malicious files were detected as attachments in email, this is a combination of <i>malware</i> and <i>phishing</i> in documents " %(o365.poc_date_begin, o365.poc_date_ended, malware365.total_of_malware_in_email)
malware_about_text['de'] = 'b'

mimetotxt = {}
mimetotxt['application/msword'] = 'MS Word'
mimetotxt['application/xml'] = 'XML'
mimetotxt['application/octet-stream'] = 'binaries'
mimetotxt['application/pdf'] = 'PDF'

#print(mimetotxt[malware365.top_malware_file_type_in_email[1]] ) 
malware_about_text_b = {}
malware_about_text_b['es'] = "Las campanias de phishing y malware responsables de estos adjuntos, los distribuyeron en %s tipos de archivos diferentes, siendo <i>%s</i>, <i>XML</i> o <i>Binarios</i> son los principales utilizados. " %(malware365.total_of_attachment_mimetype, mimetotxt['application/msword'] )

malware_about_text_b['en'] = "Phishing and Malware campaigns responsable for this attachments, had distributed them in %s different file types , being <i>%s</i>, <i>XML</i> and <i>Binaries</i> the most used." %(malware365.total_of_attachment_mimetype, mimetotxt['application/msword'] )
malware_about_text_b['de'] = 'b'

##- Top malicious files in emailn
##- This slide shows two columns with two graphics and two tables

top_malicious_files_in_email_title = {}
top_malicious_files_in_email_title['es'] = 'Top #10 archivos maliciosos en correo electronico.'
top_malicious_files_in_email_title['en'] = 'Top #10 malicious files found in Email'
top_malicious_files_in_email_title['de'] = 'x'

##- Top 20 subjects in malicous email's subject
top_malicious_subjects_title = {}
top_malicious_subjects_title['es'] = "Top 20: \"Asuntos\" comunes en correos maliciosos"
top_malicious_subjects_title['en'] = 'Top 20 Subjects used in malicious emails'
top_malicious_subjects_title['de'] = 'x'


##- Top 20 reputation of diferents feeds
top_malicious_reputation = {}
top_malicious_reputation['es'] = "Top 20: Reputacion de malware en otros feeds"
top_malicious_reputation['en'] = 'Top 20: Malware Reputation at 3rd party feeds'
top_malicious_reputation['de'] = 'x'
















##- Login analysis section

##- Transition Slide 

f456 = {}
f456['es'] = 'Robo de Identidad/Cuentas'
f456['en'] = 'Account Take-Over'
f456['de'] = 'Kontoubernahme'

##- Failed login by country

failed_logins_by_country_title = {}
failed_logins_by_country_title['es'] = 'Intentos FALLIDOS por pais fuera de %s.' % config['customer_country']
failed_logins_by_country_title['en'] = 'Top 15. Failed Logins out of %s.' % config['customer_country']
failed_logins_by_country_title['de'] = 'Top 15. Falsche Anmeldung aussserhalb %s.' % config['customer_country']

failed_logins_footer_by_country = {}
failed_logins_footer_by_country['es'] = 'Sucedieron un total de <b> %s inicios de sesion FALLIDOS de %s diferentes paises fuera de %s</b>. Estos son intentos de entrar a una cuenta donde el atacante intento adivinar las credenciales o una autenticacion de doble factor detuvo el ataque.' % (str(flogin.total_failed_login),str(flogin.total_countries_failed_login), config['customer_country'])
failed_logins_footer_by_country['en'] = 'Top 15. Failed Logins out of %s.' % config['customer_country']
failed_logins_footer_by_country['de'] = 'Top 15. Falsche Anmeldung aussserhalb %s.' % config['customer_country']

##- Failed login by city

failed_logins_by_city_title = {}
failed_logins_by_city_title['es'] = 'Intentos FALLIDOS por ciudad fuera de %s.' % config['customer_country']
failed_logins_by_city_title['en'] = 'Failed Logins out of %s by city ' % config['customer_country']
failed_logins_by_city_title['de'] = 'Top 15. Falsche Anmeldung aussserhalb %s.' % config['customer_country']

failed_logins_footer_by_city = {}
failed_logins_footer_by_city['es'] = 'Sucedieron un total de <b> %s inicios de sesion FALLIDOS de %s diferentes ciudades fuera de %s</b>.' % (str(flogin.total_failed_login),str(flogin.total_city_failed_login), config['customer_country'])
failed_logins_footer_by_city['en'] = 'Top 15. Failed Logins out of %s.' % config['customer_country']
failed_logins_footer_by_city['de'] = 'Top 15. Falsche Anmeldung aussserhalb %s.' % config['customer_country']


##- Succesfull login by country

successfull_logins_by_country_title = {}
successfull_logins_by_country_title['es'] = 'Sesiones EXITOSAS por pais fuera de %s.' % config['customer_country']
successfull_logins_by_country_title['en'] = 'SUCCESFULL Logins out of %s.' % config['customer_country']
successfull_logins_by_country_title['de'] = 'FALSCHE Anmeldung aussserhalb %s.' % config['customer_country']

succesfull_logins_footer = {}
succesfull_logins_footer['es'] = 'Sucedieron un total de <b> %s inicios de sesion EXITOSOS de %s diferentes paises fuera de %s</b>.' % (str(flogin.total_succesfull_login),str(flogin.total_countries_successfull_login), config['customer_country'])
succesfull_logins_footer['en'] = 'Top 15. SUCSSESFULL Logins out of %s.' % config['customer_country']
succesfull_logins_footer['de'] = 'Top 15. Erfolgreiche Anmeldung aussserhalb %s.' % config['customer_country']

##- Succesfull login by city

successfull_logins_by_city_title = {}
successfull_logins_by_city_title['es'] = 'Sesiones EXITOSAS por ciudad fuera de %s.' % config['customer_country']
successfull_logins_by_city_title['en'] = 'Top 15. Failed Logins out of %s.' % config['customer_country']
successfull_logins_by_city_title['de'] = 'Top 15. Falsche Anmeldung aussserhalb %s.' % config['customer_country']

succesfull_logins_by_city_footer = {}
succesfull_logins_by_city_footer['es'] = 'Sucedieron un total de <b> %s inicios de sesion EXITOSOS de %s ciudades fuera de %s</b>.' % (str(flogin.total_succesfull_login),str(flogin.total_city_successfull_login), config['customer_country'])
succesfull_logins_by_city_footer['en'] = 'Top 15. SUCSSESFULL Logins out of %s.' % config['customer_country']
succesfull_logins_by_city_footer['de'] = 'Top 15. Erfolgreiche Anmeldung aussserhalb %s.' % config['customer_country']

##- Back Cover
generation_banner = {}
generation_banner['es'] = 'Generado por - cgsreport.py - @ %s - contacto: jpadilla@checkpoint.com' % (str(time.ctime()))
generation_banner['en'] = 'Generated by - cgsreport.py - @ %s - feedback: jpadilla@checkpoint.com' % (str(time.ctime()))
generation_banner['de'] = 'Gebeuded auf - cgsreport.py - @ %s - kontact: jpadilla@checkpoint.com' % (str(time.ctime()))





